const path = require('node:path')
const express = require('express')
const app = express()
const port = 4420

app.set('views', './src/views');
app.set('view engine', 'pug');

app.use("/public", express.static(path.resolve(__dirname, 'public')));

// routes

// simple example
app.get('/', (req, res) => {
  res.render('index', {'title': 'my cool website'})
})

// more complex example
// this gathers os info and renders it as html with pug
app.get('/about', (req, res) => {
  const os = require('os')
  let info = {
    'os': os.type(),
    'ram': os.totalmem(),
    'platform': os.platform()
  }

  res.render('about', {
    'title': 'about my cool website',
    'info': info
  })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
